<?php

use \Slim\Views\PhpRenderer;

$container = $app->getContainer();
unset($container['errorHandler']);
$container['debug'] = true;
$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('my_logger');
    $file_handler = new \Monolog\Handler\StreamHandler("../logs/app.log");
    $logger->pushHandler($file_handler);
    return $logger;
};

$container['view'] = new PhpRenderer("../templates/");


