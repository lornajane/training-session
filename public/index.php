<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require "../vendor/autoload.php";

$app = new \Slim\App;
// configure container dependencies: view, logger, etc
require "../container.php";

// create a simple exception handler
set_exception_handler(function ($exception) use ($app) {
    $logger = $app->getContainer()['logger'];
    $logger->addError($exception->getMessage());
    http_response_code(404);
    $debug = $app->getContainer()['debug'];
    include("../templates/404.php");
});


$app->get('/hello/{name}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $response->getBody()->write("Hello, $name");

    return $response;
});

$app->get('/', function (Request $request, Response $response) {
    $this->logger->addInfo("Main route loaded");
    $response = $this->view->render($response, "home.php");
    return $response;
});

$app->post('/greet', function (Request $request, Response $response) {
    $form_data = $request->getParsedBody();
    $this->logger->addInfo("Greet route", $form_data);

    $data = [];
    $data['name'] = filter_var($form_data['name'], FILTER_SANITIZE_STRING);

    $response = $this->view->render($response, "greet.php", $data);
    return $response;
});

$app->run();
